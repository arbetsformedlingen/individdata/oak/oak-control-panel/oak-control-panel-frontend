# oak-control-panel

A ClojureScript prototype for the Oak Control Panel .

## build prerequisite
The project uses [Leiningen](https://leiningen.org/) as a build tool

## development
Start the development environment with
<pre>
lein watch
</pre>
and go to http://localhost:8280/index.html in your preferred browser.
(You can change the port number in the shadow-cljs.edn file)

## IntelliJ IDEA
Install the [Calva](https://calva.io/) pluigin for Clojure support.

## release build
Release files are built with
<pre>
lein release
</pre>
and will result in minified JavaScript application file.

