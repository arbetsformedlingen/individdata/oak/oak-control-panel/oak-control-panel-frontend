(defproject oak-control-panel "lein-git-inject/version"

  :dependencies [[org.clojure/clojure "1.10.1"]
                 [org.clojure/clojurescript "1.10.773"
                  :exclusions [com.google.javascript/closure-compiler-unshaded
                               org.clojure/google-closure-library
                               org.clojure/google-closure-library-third-party]]
                 [thheller/shadow-cljs "2.11.4"]
                 [reagent "0.10.0"]
                 [re-frame "1.1.2"]
                 [day8.re-frame/http-fx "0.2.2"]
                 [cljs-ajax "0.8.0"]
                 [net.cgrand/macrovich "0.2.1"]
                 [org.clojure/tools.logging "0.4.1"]
                 [metosin/reitit "0.5.11"]                  ; Routing
                 [metosin/reitit-malli "0.5.11"]            ; Coercion
                 [ring/ring-jetty-adapter "1.7.1"]          ; Web server
                 [com.andrewmcveigh/cljs-time "0.5.2"]
                 ]
  :plugins [
            [lein-shadow "0.3.1"]]

  :source-paths ["src"]

  :clean-targets ^{:protect false} [:target-path
                                    "shadow-cljs.edn"
                                    "node_modules"
                                    "resources/public/js"]

  :shadow-cljs {:nrepl  {:port 9631}

                :builds {:app {
                               :target     :browser
                               :output-dir "resources/public/js"
                               :modules    {:app {:init-fn se.jobtechdev.oak.cpl.core/run}}
                               :dev        {:closure-defines {}}
                               :devtools   {:http-root "resources/public"
                                            :http-port 8281}
                               :release    {}}}}

  :aliases {"watch"        ["do"
                            ["clean"]
                            ["shadow" "watch" "app"]]

            "release"      ["do"
                            ["clean"]
                            ["shadow" "release" "app"]]

            "build-report" ["do"
                            ["clean"]
                            ["shadow" "run" "shadow.cljs.build-report" "app" "target/build-report.html"]]})
