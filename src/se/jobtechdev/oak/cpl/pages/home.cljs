(ns se.jobtechdev.oak.cpl.pages.home
  (:require
   [re-frame.core :as rf]
   [se.jobtechdev.oak.cpl.auth :as auth]
   [se.jobtechdev.oak.cpl.oak :as oak]))

(defn ui-login-logout-btn []
  (let [authorized @(rf/subscribe [::auth/authorized?])
        caption (if authorized "Logout" "Login")
        disp (if authorized
               ::oak/logout
               ::auth/login)]
    [:a
     [:div#login {:onClick #(rf/dispatch [disp])} caption]]))

(defn request-list0 []
      (when @(rf/subscribe [::auth/authorized?])
        [:<>
         [:td
          [:table
           [:tbody
            #_[:tr [:td [:b "WebID: "] @(rf/subscribe [::auth/web-id])]]
            #_[:tr [:td [:b "Name: "] @(rf/subscribe [::auth/name])]]
            #_[:tr
             [:td [:button {:onClick #(rf/dispatch [::oak/get-data-requests])}
                   "Get DataRequest"]]
             [:td [:button {:onClick #(rf/dispatch [::oak/allow-data-request-all])}
                   "Consent all"]]
             [:td [:button {:onClick #(rf/dispatch [::oak/empty-inbox])}
                   "Empty inbox"]]]]]]
         (when @(rf/subscribe [::oak/data-requests])
           [:<>
            [:tr

             [:td "DataRequests: "]]
            [:tr [:td
                  (doall
                   (for [[id dr] @(rf/subscribe [::oak/data-requests])]
                     ^{:key id}
                     [:table {:style {:border "black solid 1px"}}
                      [:tbody
                       (doall
                        (let [dr (:content dr)]
                          (for [[k v] dr]
                            ^{:key (str id k)} [:tr [:td k] [:td v]])))
                       [:tr
                        [:td
                         (let [consented @(rf/subscribe [::oak/data-request-consented id])]
                           [:button {:disabled consented
                                     :onClick #(rf/dispatch [::oak/allow-data-request {:id id}])}
                            (if consented "Consented" "Consent")])]]]]))]]])]))

(defn items-list []
  [:<>
   (let [data-items @(rf/subscribe [::oak/data-items])]
     (if data-items
       [:<>
        (doall
         (for [[id {:keys [data-request data-response]}] data-items]
           (if data-response
             (let [{:keys [status subject statusChangedDate issuer]} (:credentialSubject data-response)
                   {issuer-name :name} issuer]
               ^{:key id}
               [:div.content
                [:div.description [:span "Försäkringsbolaget "] [:span.bold " Sink"] [:span " har begärt denna data om dig från "] [:span.bold "Arbetsförmedlingen"]]
                [:div.row [:div.input [:span "Datum: "] [:span.green statusChangedDate]]]
                [:div.row [:div.input [:span "Anställningsstatus: "] [:span.green status]]]
                [:div.row [:div.input [:span "Personnummer: "] [:span.green subject]]]
                #_[:div.row [:div.input [:span "Datakälla: "] [:span.green issuer-name]]]
                [:div.row
                 [:div.status
                  [:span [:div.check-circle]]
                  [:span "Data hämtad"]]]
                [:div.row
                 (let [consented @(rf/subscribe [::oak/data-response-consented id])
                       shared @(rf/subscribe [::oak/data-shared id])]
                   (if shared
                     [:div.status1
                      [:span [:div.check-circle]]
                      [:span "Data delad"]]
                     [:div.status1
                      [:span.high [:div {:class (if consented "check-square" "square") :onClick #(rf/dispatch [::oak/consent-data-response {:id id :value (not consented)}])}]]
                      [:button {:class (if consented "green" "grey")
                                :disabled (not consented)
                                :onClick #(rf/dispatch [::oak/share-data-response {:id id}])}
                       "Dela data"]]))]])
             (let [{:keys [requested-data]} data-request]
               ^{:key id}
               [:div.content
                [:div.description [:span "Försäkringsbolaget "] [:span.bold " Sink"] [:span " har begärt denna data om dig från "] [:span.bold "Arbetsförmedlingen"]]
                [:div.row [:div.input [:span "Datum"]]]
                [:div.row [:div.input [:span "Anställningsstatus"]]]
                [:div.row [:div.input [:span "Personnummer"]]]
                #_[:div.row [:div.input [:span "Datakälla"]]]
                [:div.row
                 (let [consented @(rf/subscribe [::oak/data-request-consented id])]
                   [:button.button {:disabled consented
                                             :onClick #(rf/dispatch [::oak/allow-data-request {:id id}])}
                    [:span (if consented "Hämtar data" "Hämta data")]
                    [:span (if consented [:div.arrow-down.loading] [:div.arrow-down])]])]]))))]
       [:div.content
        [:div.description "Du har inga aktiva begäran om datadelningar"]]))])

(defn home-page []
  [:div.page
   [:header
    [:nav
     [:a {:href "#"} [:img#image {:src (str "/img/oak.svg") :alt "Oak image" :width "50" :height "50"}]]
     [:a#title {:href "#"}
      [:div#title
       [:span "Project"]
       [:span.green "OAK"]]]
     [ui-login-logout-btn]]]
   (when @(rf/subscribe [::auth/authorized?])
     [:div.container
      [:p "Hej " @(rf/subscribe [::auth/name]) "!"]
      [items-list]]
   )  
   [:footer]])
    
