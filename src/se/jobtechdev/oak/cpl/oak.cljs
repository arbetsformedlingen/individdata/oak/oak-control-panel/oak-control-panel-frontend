(ns se.jobtechdev.oak.cpl.oak
  (:require
   [clojure.pprint :as pp]
   [re-frame.core :as rf]
   [se.jobtechdev.oak.cpl.solid :as solid]
   [se.jobtechdev.oak.cpl.helpers :as helpers]
   [se.jobtechdev.oak.cpl.auth :as auth]))

(defn storage-url [db & args]
  (str (get-in db [::auth/auth :storage]) (apply str args)))

(defn current-ms []
  (js/Date.now))

(defn enrich-before
  [f]
  (rf/->interceptor
   :id :enrich-before
   :before (fn enrich-before
             [context]
             (let [event (rf/get-coeffect context :event)
                   db    (rf/get-coeffect context :db)]
               (->> (f db event)
                    (rf/assoc-coeffect context :db))))))

(def start-timer
  (enrich-before (fn [db [_ {:keys [id]}]]
                   (js/console.log "Start Timer" (current-ms) id)
                   (assoc-in db [::start-time id] (current-ms)))))

(def stop-timer
  (rf/enrich (fn [db [_ {:keys [id]}]]
               (let [duration (- (current-ms) (get-in db [::start-time id]))]
                 (js/console.log "Duration" duration)
                 (-> db
                     (assoc-in [::duration id] duration)
                     #_(dissoc ::start-time))))))

(comment
  (get-in @re-frame.db/app-db [::duration])
  (get-in @re-frame.db/app-db [::start-time]))

;; Inbox

(rf/reg-event-fx
 ::get-inbox
 [start-timer rf/debug]
 (fn [{db :db} [_]]
   (js/console.log "get-inbox")
   {#_:db #_(dissoc db ::data-requests)
    :fx [[::solid/get-solid-dataset! {:url (storage-url db "oak/inbox/")
                                      :on-success [::get-inbox-content]
                                      :on-error [::handle-error]}]]}))

(rf/reg-event-fx
 ::handle-error
 [rf/debug]
 (fn [{db :db} [_ {:keys [error]}]]
   (let [err (js->clj error)]
     (def err err)
     (js/console.log "::handle-error" err))
   ))

(rf/reg-event-fx
 ::get-inbox-content
 rf/debug
 (fn [{db :db} [_ {:keys [url dataset]}]]
   (let [content-list (-> (solid/get-thing dataset url)
                          (solid/get-url-all "http://www.w3.org/ns/ldp#contains"))]
     {:fx (for [content content-list]
            [::solid/get-solid-dataset! {:url content
                                         :on-success [::handle-content]}])})))

(rf/reg-event-fx
 ::handle-content
 rf/debug
 (fn [_ [_ {:keys [url dataset]}]]
   (let [thing (solid/get-thing dataset url)
         data-request-url (solid/get-url thing "https://oak.se/dataRequest")
         data-response-url (solid/get-url thing "https://oak.se/dataResponseUrl")]
     {:fx [[::solid/get-solid-dataset!
            (if data-request-url
              {:url data-request-url
               :on-success [::get-data-request]}
              {:url data-response-url
               :on-success [::get-data-response]})]]})))

(rf/reg-event-fx
 ::get-agent-info
 rf/debug
 (fn [_ [_ {:keys [url dataset]}]]
   (let [data-request-url (-> (solid/get-thing dataset url)
                              (solid/get-url "https://oak.se/dataRequest"))]
     {:fx [[::solid/get-solid-dataset! {:url data-request-url
                                        :on-success [::get-data-request-success]}]]})))


(rf/reg-event-db
 ::get-data-request
 [stop-timer rf/debug]
 (fn [db [_ {:keys [url dataset]}]]
   (let [t (solid/get-thing dataset url)
         id (solid/get-string-no-locale t "http://schema.org/identifier")
         requested-by (solid/get-url t "http://www.w3.org/2007/ont/link#requestedBy")
         m {:id id
            :subject (solid/get-url t "http://purl.org/dc/elements/1.1/subject")
            :pnr (solid/get-string-no-locale t "https://oak.se/subjectId")
            :requested-data (solid/get-string-no-locale-all t "https://oak.se/requestedData")
            :requested-from (solid/get-url t "http://schema.org/sourceOrganization")
            :requested-by requested-by
            :type (solid/get-url t "http://www.w3.org/1999/02/22-rdf-syntax-ns#type")}
         ]
     (-> db
         (assoc-in [::data-items id :requested-by] requested-by)
         (assoc-in [::data-items id :data-request] m)
         ))))

(comment
  (def decoded (js/atob "")))

(rf/reg-event-fx
 ::get-data-response
 [rf/debug]
 (fn [{db :db} [_ {:keys [url dataset]}]]
   (let [t (solid/get-thing dataset url)
         id (solid/get-string-no-locale t "urn:id")
         vcs (solid/get-string-no-locale t "http://schema.org/text")
         vc (js->clj (.parse js/JSON vcs) :keywordize-keys true)
         shared-data-url (storage-url db "oak/shared/shared-data-" id)
         ]
     {}
     {:fx [[::solid/get-resource-info! {:url shared-data-url
                                        :on-success [::data-shared {:id id :value true}]
                                        :on-error [::data-shared {:id id :value false}]}]]
      :db (-> db
              (assoc-in [::data-items id :data-response-url] url)
              (assoc-in [::data-items id :data-response] vc))}
     
     #_(assoc-in db [::data-items id :data-response] vc)
     )))

(comment
  (def vc) (js->clj (.parse js/JSON "{\"a\": 1}"))


  (pp/pprint @re-frame.db/app-db)
  )



(rf/reg-sub ::data-items
            (fn [db _]
              (get-in db [::data-items])))


(rf/reg-sub ::data-request-consented
            (fn [db [_ id]]
              (get-in db [::data-items id :consented])))

(rf/reg-event-db
 ::consent-data-response
 [rf/debug]
 (fn [db [_ {:keys [id value]}]]
   (assoc-in db [::data-items id :response-consented] value)))


(rf/reg-sub ::data-response-consented
            (fn [db [_ id]]
              (get-in db [::data-items id :response-consented])))


;; Allow

(defn data-request-consent
  [{:keys [id response-url pnr user source sink]}]
  (let [thing
        (-> (solid/build-thing (solid/create-thing))
            (.addUrl "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" "http://oak.se/UnemploymentCertificateDataRequest")
            (.addStringNoLocale "http://schema.org/identifier" id)
            (.addUrl "https://oak.se/dataResponseUrl" response-url)
            #_(.addUrl "https://oak.se/dataResponseLinkUrl" response-link-url)
            (.addUrl "http://purl.org/dc/elements/1.1/subject" user)
            (.addStringNoLocale "https://oak.se/subjectId" pnr)
            (.addUrl "http://schema.org/sourceOrganization" source)
            (.addUrl "http://www.w3.org/2007/ont/link#requestedBy" sink)
            (.build))]
    (solid/set-thing (solid/create-solid-dataset) thing)))

(defn data-request-consent-link
  [request-url]
  (let [thing
        (-> (solid/build-thing (solid/create-thing))
            (.addUrl "https://oak.se/dataRequest" request-url)
            (.build))]
    (solid/set-thing (solid/create-solid-dataset) thing)))

(defn data-response-share-link
  [response-url]
  (let [thing
        (-> (solid/build-thing (solid/create-thing))
            (.addUrl "https://oak.se/dataResponseUrl" response-url)
            (.build))]
    (solid/set-thing (solid/create-solid-dataset) thing)))

(def delay-atom (atom 0))

(def mean 600)

(defn poisson [mean max]
  (let [p (* (- (js/Math.log (- 1 (js/Math.random)))) mean)]
    (if (> p max) max p)))

(rf/reg-fx
 :random-delay
 (fn [event]
   (js/setTimeout
    (fn []
      (js/console.log "Dispatch" (str event))
      (rf/dispatch event))
    (swap! delay-atom #(+ %1 (poisson mean 2000)) @delay-atom)
    #_(* (swap! delay-atom inc)
         #_400
         (poisson mean 2000))
    #_(* (- (js/Math.log (- 1 (js/Math.random)))) mean)
    #_(* (js/Math.random) 1))))

(rf/reg-event-fx
 ::allow-data-request-all
 rf/debug
 (fn [{db :db}]
   (let [requests (get-in db [::data-requests])]
     {:fx (for [[id _] requests]
            [:random-delay [::allow-data-request {:id id}]])})))

(comment)

(rf/reg-event-fx
 ::allow-data-request
 [start-timer rf/debug]
 (fn [{db :db} [_ {:keys [id]}]]
   (let [{:keys [requested-from]} (get-in db [::data-items id :data-request])]
     (if requested-from
       {:db db
      ;; get the profile of the source
        ::solid/get-solid-dataset!
        {:url requested-from
         :on-success [::allow-data-request-2 {:id id}]}}
       {:db db}
       ))))

(rf/reg-event-fx
 ::allow-data-request-2
 rf/debug
 (fn [{db :db} [_ {:keys [id url dataset]}]]
   (let [user (get-in db [::auth/auth :web-id])
         {:keys [requested-from requested-by pnr]} (get-in db [::data-items id :data-request])
         req-perm [{:agent user :access #{:read :write :control}}
                   {:agent requested-from :access #{:read}}]
         resp-perm [{:agent user :access #{:read :write :control}}
                    {:agent requested-from :access #{:write}}
                    {:agent requested-by :access #{:read}}]
         consent-url (storage-url db "oak/consents/consent-" id)
         response-url (storage-url db "oak/responses/response-" id)
         #_response-link-url #_(str "https://css2-ipo-dev.test.services.jtech.se/sink/oak/inbox/response-link" id)
         consent-link-url (solid/profile->storage-url url dataset "oak/inbox/consent-link-" id)
         on-complete [::solid/set-solid-dataset
                      {:url consent-link-url
                       :ds (data-request-consent-link (storage-url db "oak/consents/consent-" id))
                       :on-success [::allow-data-request-success {:id id}]}]]
     {:db (assoc-in db [::data-requests id ::wait-for-completion] #{consent-url response-url})
      :fx
      [[::solid/set-solid-dataset!
        {:url consent-url
         :ds (data-request-consent {:id id
                                    :response-url response-url
                                    #_:response-link-url #_response-link-url
                                    :pnr pnr
                                    :user user
                                    :source requested-from
                                    :sink requested-by})
         :on-success [::solid/set-solid-permissions
                      {:permissions req-perm
                       :on-success [::complete-part {:id id
                                                     :part-id consent-url
                                                     :on-complete on-complete}]}]}]
       [::solid/set-solid-dataset!
        {:url response-url
         :on-success [::solid/set-solid-permissions
                      {:permissions resp-perm
                       :on-success [::complete-part {:id id
                                                     :part-id response-url
                                                     :on-complete on-complete}]}]}]]})))

(defn dissoc-in
  "Dissociates an entry from a nested associative structure returning a new
  nested structure. keys is a sequence of keys. Any empty maps that result
  will not be present in the new structure."
  [m [k & ks :as keys]]
  (if ks
    (if-let [nextmap (get m k)]
      (let [newmap (dissoc-in nextmap ks)]
        (if (seq newmap)
          (assoc m k newmap)
          (dissoc m k)))
      m)
    (dissoc m k)))

(rf/reg-event-fx
 ::complete-part
 rf/debug
 (fn [{db :db} [_ {:keys [id part-id on-complete]}]]
   (let [wait-for-completion (get-in db [::data-requests id ::wait-for-completion])
         w (disj wait-for-completion part-id)]
     (if (empty? w)
       {:db (dissoc-in db [::data-requests id ::wait-for-completion])
        :dispatch on-complete}
       {:db (assoc-in db [::data-requests id ::wait-for-completion] w)}))))

(rf/reg-event-fx
 ::remove-consented-request
 rf/debug
 (fn [{db :db} [_ {:keys [id on-success]}]]
   {::solid/delete-solid-dataset! {:url (storage-url db "oak/inbox/request-link-" id)
                                   :on-success on-success
                                   :on-error on-success}}))

(rf/reg-event-db
 ::allow-data-request-success
 [stop-timer rf/debug]
 (fn [db [_ {:keys [id] :as ev}]]
   (assoc-in db [::data-items id :consented] true)))

(rf/reg-sub
 ::permission-given
 (fn [db [_ id]]
   (::data-request-success db)))

;; Share Data

(rf/reg-event-fx
 ::share-data-response
 rf/debug
 (fn [{db :db} [_ {:keys [id]}]]
   (let [requested-by (get-in db [::data-items id :requested-by])
         data-response-url (get-in db [::data-items id :data-response-url])]
     (js/console.log (str "::share-data-response id=" id " requeted-by=" requested-by " data-response-url=" data-response-url))
     (if requested-by
       {:db db
      ;; get the profile of the sink
        ::solid/get-solid-dataset!
        {:url requested-by
         :on-success [::share-data-response-2 {:id id}]}}
       {:db db}))))

(rf/reg-event-fx
 ::share-data-response-2
 [rf/debug]
 (fn [{db :db} [_ {:keys [id url dataset]}]]
   (let [data-response-url (get-in db [::data-items id :data-response-url])
         response-link-url (solid/profile->storage-url url dataset "oak/inbox/response-link-" id)
         shared-data-url (storage-url db "oak/shared/shared-data-" id)]
     {:db db
      :fx
      [[::solid/set-solid-dataset!
        {:url response-link-url
         :ds (data-response-share-link data-response-url)
         :on-success [::data-shared {:id id :value true}]}]
       [::solid/set-solid-dataset!
        {:url shared-data-url
         :on-success [::data-shared {:id id :value true}]}]]})))

(rf/reg-event-db
 ::data-shared
 rf/debug
 (fn [db [_ {:keys [id value]}]]
   (assoc-in db [::data-items id :data-shared] value)))

(rf/reg-sub ::data-shared
            (fn [db [_ id]]
              (get-in db [::data-items id :data-shared])))


;; Empty Inbox

(rf/reg-event-fx
 ::logout
 rf/debug
 (fn [_ _]
   {:fx [[:dispatch [::stop-inbox-polling]]
         [:dispatch [::auth/logout]]]}))

(rf/reg-event-fx
 ::empty-inbox
 rf/debug
 (fn [{db :db} [_]]
   {:fx [[::solid/get-solid-dataset! {:url (storage-url db "oak/inbox/")
                                      :on-success [::empty-inbox-content]}]]}))

(rf/reg-event-fx
 ::empty-inbox-content
 rf/debug
 (fn [_ [_ {:keys [url dataset]}]]
   (let [active-list (-> (solid/get-thing dataset url)
                         (solid/get-url-all "http://www.w3.org/ns/ldp#contains"))]
     {:fx (for [active active-list]
            [::solid/get-solid-dataset! {:url active
                                         :on-success [::empty-inbox-delete]}])})))

(rf/reg-event-fx
 ::empty-inbox-delete
 rf/debug
 (fn [_ [_ {:keys [url]}]]
   {:fx [[::solid/delete-solid-dataset! {:url url
                                         :on-success [::empty-inbox-success]}]]}))

(rf/reg-event-db
 ::empty-inbox-success
 rf/debug
 (fn [db _]
   (dissoc db ::data-requests)))

;; inbox polling

(rf/reg-event-db
 ::start-inbox-polling
 rf/debug
 (fn [db _]
   (if (auth/authorized? db nil)
     (do
       (js/console.log "Start polling of inbox")
       (rf/dispatch [::get-inbox])
       (assoc db ::inbox-polling-timer (helpers/set-interval #(rf/dispatch [::get-inbox]) 10000)))
     db)))

(rf/reg-event-db
 ::stop-inbox-polling
 rf/debug
 (fn [db _]
   (js/console.log "Stop polling of inbox")
   (let [id (get-in db [::inbox-polling-timer])]
     (helpers/clear-interval id)
     (dissoc db ::inbox-polling-timer))))


(comment

  (do (defn mean [xs]
        (/ (reduce + xs) (count xs)))
      (defn mmin [xs]
        (first (sort xs)))
      (defn mmax [xs]
        (last (sort xs)))
      (defn stats [xs]
        {:mean (mean xs)
         :min (mmin xs)
         :max (mmax xs)
         :sum (reduce + xs)
         :count (count xs)})

      (stats (vals (dissoc (::duration @re-frame.db/app-db) nil))))



  ;; With debug... :(

  ;; 10 samples, with 0-2s delay
  {:mean 2030, :min 1183, :max 2535}
  ;; 10 samples without delay
  {:mean 3169.9, :min 3013, :max 3309}
  ;; 100 samples with 1 s delay
  {:mean 759.4285714285714, :min 354, :max 4629}
  ;; 100 samples with 300 ms delay
  {:mean 25739.519607843136, :min 692, :max 35359}
  ;; 100 samples with 500 ms delay
  {:mean 29850.241379310344, :min 412, :max 57122}

  ;; 100 samples with 2000 ms delay

  ;; Without debug...
  ;; 100 samples with 0 ms delay
  {:mean 165979.32, :min 131036, :max 177763, :sum 16597932, :count 100}

  ;; 100 samples with 100 ms delay
  {:mean 136391.53, :min 122021, :max 139019, :sum 13639153, :count 100}

  ;; 100 samples with 500 ms delay (after chrasch restart...)
  {:mean 259.38, :min 225, :max 641, :sum 25938, :count 100}

  ;; 100 samples with 400 ms delay (after chrasch restart... -422 ???)
  {:mean 311.91, :min -422, :max 772, :sum 31191, :count 100}

  ;; laptop chrasches a third time... websense

  ;; and a fourth time ..... looks lite 350 ms always chrasch the laptop ... but why?

  ;; 100 samples with 300 ms delay (after fourth chrasch restart...)
  {:mean 1388.28, :min 502, :max 2016, :sum 138828, :count 100}

  ;; after performance refactoring of canHandle (try/catch -> Answer<?,Error>)

  ;; 100 samples with poisson distribution 400 ms mean, 1000 max delay
  {:mean 16153.06, :min 9471, :max 21516, :sum 1615306, :count 100}

  ;; 100 samples with poisson distribution 600 ms mean, 2000 max delay
  {:mean 745.02, :min 167, :max 2116, :sum 74502, :count 100}

  ;; 100 samples with 400 ms delay, production mode
  {:mean 252.62, :min 201, :max 493, :sum 25262, :count 100}

  ;; 200 samples with 400 ms delay, after removing some more try/catch
  {:mean 429.515, :min 189, :max 786, :sum 85903, :count 200}

  ;; removing componentsjs ;;

  ;; 200 samples with 400 ms delay
  {:mean 467.41, :min 178, :max 925, :sum 93482, :count 200}

  ;; 100 samples with poisson distribution 500 ms mean, 2000 max delay, production mode
  {:mean 745.02, :min 167, :max 2116, :sum 74502, :count 100}

  ;; 100 samples with poisson distribution 600 ms mean, 2000 max delay, production mode
  )



