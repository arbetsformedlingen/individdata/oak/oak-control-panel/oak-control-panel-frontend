(ns se.jobtechdev.oak.cpl.local-storage
  (:require
   [re-frame.core :as rf]))

(def local-storage ^js/Storage js/localStorage)

(defn contains-key?
  [key]
  (let [ks (.keys js/Object local-storage)
        idx (.indexOf ks (name key))]
    (>= idx 0)))

(defn get-item
  ([key]
   (get-item key nil))
  ([key ^string default]
   (if (contains-key? key)
     (.getItem local-storage (name key))
     default)))

(defn set-item
  [key ^string val]
  (.setItem local-storage (name key) val)
  local-storage)

(defn remove-item
  [key]
  (.removeItem local-storage (name key))
  local-storage)

;; State
(comment
  (rf/reg-event-fx
    ::persist
    rf/debug
    (fn
      [_ [_ item]]
      {::persist-fx item}))

  (rf/reg-fx
    ::persist-fx
    (fn [[key value]]
      (.set-item local-storage key value)))

  (rf/reg-sub ::loading?
              (fn [db _]
                (boolean (get db ::loading))))

  (rf/reg-sub ::authorized?
              (fn [db _]
                (boolean (get-in db [::authorization :accessToken]))))

  )
