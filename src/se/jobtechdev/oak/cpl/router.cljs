(ns se.jobtechdev.oak.cpl.router
  (:require
    [re-frame.core :as rf]
    [reitit.frontend]
    [reitit.frontend.easy :as rfe]
    [reitit.frontend.controllers :as rfc]
    [reitit.core :as r]))

;; Save a handle to the router when it's initialized.
(defonce router (atom nil))

;;; Subs

(rf/reg-sub
  :current-route
  (fn [db path]
    (get-in db path)))

;;; Events

(rf/reg-event-fx
  :navigate
  (fn [_cofx [_ & route]]
    {:navigate! route}))

(rf/reg-event-db
  :navigated
  (fn [db [_ new-match]]
    (let [old-match (:current-route db)
          controllers (rfc/apply-controllers (:controllers old-match) new-match)]
      (assoc db :current-route (assoc new-match :controllers controllers)))))

;; Triggering navigation from events.
(rf/reg-fx
  :navigate!
  (fn [route]
    (apply rfe/push-state route)))

(defn match-by-name [name]
  (when @router
    (r/match-by-name @router name)))

(defn on-navigate [new-match]
  (when new-match
    (rf/dispatch [:navigated new-match])))

(defn init-routes! [routes]
  (let [new-router (reset! router (reitit.frontend/router routes))]
    (rfe/start!
      new-router
      on-navigate
      ;; mds-core needs to use fragments for the callback, so we can't use it for routing
      {:use-fragment false})))
