(ns se.jobtechdev.oak.cpl.app
  (:require
   [day8.re-frame.http-fx]
   [re-frame.core :as rf]
   [se.jobtechdev.oak.cpl.pages.home :as home]
   [se.jobtechdev.oak.cpl.auth :as auth]
   [se.jobtechdev.oak.cpl.oak :as oak]))

;; sets up initial application state
(rf/reg-event-db :initialize (fn [_ _] {}))

(def routes
  ["/"
   [""
    {:name        :home
     :view        home/home-page
     :controllers
     [{:start (fn [& params] (rf/dispatch [::oak/start-inbox-polling]))
       :stop (fn [& params] (js/console.log "Leaving home page"))}]}]
 ["login"
  {:name        :login-callback
   :view        #()                                       ; No view: the controller will redirect
   :controllers [{:start auth/login-callback}]}]])

(defn root
  []
  (let [current-route @(rf/subscribe [:current-route])]
    (when current-route
      [(get-in current-route [:data :view])])))

(comment
  
  (+ 1 1)
  
  )