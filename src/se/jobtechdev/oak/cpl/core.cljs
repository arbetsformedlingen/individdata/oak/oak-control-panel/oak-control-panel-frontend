(ns se.jobtechdev.oak.cpl.core
  (:require
   [reagent.dom :as dom]
   [re-frame.core :as rf]
   [se.jobtechdev.oak.cpl.app :as app]
   [se.jobtechdev.oak.cpl.router :as routes]))

(defn render
  []
  (dom/render [app/root]
              (.getElementById js/document "app")))

(defn run
  []
  (rf/dispatch-sync [:initialize])                          ; put a value into application state
  (routes/init-routes! app/routes)
  (render)                                                  ; mount the application's ui into '<div id="app" />'
  )

(defn ^:dev/after-load clear-cache-and-render!
  []
  ;; The `:dev/after-load` metadata causes this function to be called
  ;; after shadow-cljs hot-reloads code. We force a UI update by clearing
  ;; the Reframe subscription cache.
  ;;(rf/clear-subscription-cache!)
  (render))

(comment
  (keys @re-frame.db/app-db)

  (:se.jobtechdev.oak.cpl.oak/data-requests @re-frame.db/app-db)
  
  )
