(ns se.jobtechdev.oak.cpl.auth
  (:require
   [re-frame.core :as rf]
   [se.jobtechdev.oak.cpl.solid :as solid]
   [se.jobtechdev.oak.cpl.local-storage :as lstor]))

(defn save-route
  "Store the current route in local storage, so we can return to it after login"
  [current-route]
  (lstor/set-item :after-login-route (name current-route)))

(defn restore-route
  "Navigate to the route stored in local storage"
  [params]
  (let [destination (or (keyword (lstor/get-item :after-login-route)) :home)]
    (lstor/remove-item :after-login-route)
    (rf/dispatch [:navigate destination params])))

(defn login-callback []
  (rf/dispatch [::waiting-for-auth])
  (solid/handle-incoming-redirect {:on-success #(restore-route {})}))

;; State

(rf/reg-fx
 ::login!
 (fn [{:keys [current-route]}]
   (save-route current-route)
   (solid/perform-authentication)))

(rf/reg-event-fx
  ::login
  rf/debug
  (fn [{db :db} _]
    {::login! {:current-route (get-in db [:current-route :data :name])}}))

(rf/reg-event-db
 ::waiting-for-auth
 rf/debug
 (fn [db _]
   (assoc db ::waiting-for-auth true)))

(rf/reg-fx
 ::logout!
 (fn [_]
   (solid/logout)))

(rf/reg-event-fx
 ::logout
 rf/debug
 (fn [_ _]
     {:fx [[::logout!]
           [:dispatch [:initialize]]
           [:navigate! [:home]]]}))

(rf/reg-event-db
 ::save
 rf/debug
 (fn [db [_ m]]
   (assoc db ::auth m)))

(defn authorized?
  [db _]
  (boolean (get-in db [::auth])))

(rf/reg-sub ::authorized? authorized?)

(rf/reg-sub ::waiting-for-auth?
            (fn [db _]
              (boolean (and (::waiting-for-auth db) (not (::profile db))))))

(rf/reg-sub ::name
            (fn [db _]
              (get-in db [::auth :name])))

(rf/reg-sub ::web-id
            (fn [db _]
              (get-in db [::auth :web-id])))
