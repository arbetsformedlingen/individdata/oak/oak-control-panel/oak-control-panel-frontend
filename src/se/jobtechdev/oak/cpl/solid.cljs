(ns se.jobtechdev.oak.cpl.solid
  (:require
   [re-frame.core :as rf]
   [se.jobtechdev.oak.cpl.helpers :as helpers]
   ["@inrupt/solid-client" :as client]
   ["@inrupt/solid-client-authn-browser" :as client-auth]))

(def issuer "http://localhost:3000/")
#_(def issuer "https://css2-ipo-dev.test.services.jtech.se/")

;; Interop

(def build-thing client/buildThing)
(def create-solid-dataset client/createSolidDataset)
(def create-thing client/createThing)
(def get-string-no-locale client/getStringNoLocale)
(def get-string-no-locale-all client/getStringNoLocaleAll)
(def get-thing client/getThing)
(def get-url client/getUrl)
(def get-url-all (comp js->clj client/getUrlAll))
(def set-thing client/setThing)

;; Solid auth

(defn perform-authentication []
  (client-auth/login #js {:oidcIssuer  issuer
                          :redirectUrl (str (helpers/current-origin) "/login")
                          :clientName  "Oak Control Panel"}))

(defn handle-incoming-redirect [{:keys [on-success]}]
  (.then (client-auth/handleIncomingRedirect #js {:restorePreviousSession false})
         (fn [_]
           (let [info (some-> (client-auth/getDefaultSession) (.-info))]
             (when (.-isLoggedIn info)
               (let [web-id (.-webId info)]
                 (.then (client/getSolidDataset web-id)
                        (fn [dataset]
                          (let [profile (client/getThing dataset web-id)
                                name (client/getStringNoLocale profile "http://xmlns.com/foaf/0.1/name")
                                storage (client/getUrl profile "http://www.w3.org/ns/pim/space#storage")]
                            (rf/dispatch [:se.jobtechdev.oak.cpl.auth/save {:name name
                                                                            :web-id web-id
                                                                            :storage storage}])
                            (on-success))))))))))

(defn logout []
  (client-auth/logout))

;; Helpers

(defn ->solid-access
  [access]
  (clj->js (cond-> {}
             (:read access) (assoc :read true)
             (:append access) (assoc :append true)
             (:write access) (assoc :write true)
             (:control access) (assoc :control true))))

(defn permissions->acl
  [resource permissions]
  (reduce (fn [acl {:keys [agent access]}]
            (client/setAgentResourceAccess acl agent (->solid-access access)))
          (client/createAcl resource)
          permissions))

(defn merge-event-params [event new-params]
  (let [[event-name params] event]
    [event-name (merge params new-params)]))


(defn profile->storage-url [web-id profile-ds & args]
  (str (get-url (get-thing profile-ds web-id) "http://www.w3.org/ns/pim/space#storage") (apply str args)))

;; Dataset

(rf/reg-fx
 ::get-solid-dataset!
 (fn [{:keys [url on-success on-error]}]
   (js/console.log "::get-solid-dataset!")
   (-> (client/getSolidDataset url
                               #js {:fetch client-auth/fetch})
    (.then (fn [dataset]
             (js/console.log "::get-solid-dataset! then" url dataset)
             (rf/dispatch (merge-event-params on-success {:url url
                                                          :dataset dataset}))))
    (.catch (fn [error]
              (js/console.log "::get-solid-dataset! catch" error)
            (rf/dispatch (merge on-error {:error error})))))
    ))

(comment
  (.then (client/getSolidDataset "http://localhost:3000/user/oak/active-requests/"
                                 #js {:fetch client-auth/fetch})
         (fn [dataset]
           (def ds dataset)))
  ds

  (def r (get-thing ds "http://localhost:3000/user/oak/active-requests/"))

  (get-url-all r "http://www.w3.org/ns/ldp#contains")

  (.then (client/getSolidDataset (first *1)
                                 #js {:fetch client-auth/fetch})
         (fn [dataset]
           (def ds2 dataset)))
  
  (-> (get-thing ds2 "http://localhost:3000/user/oak/active-requests/active-request-e6cfe2c2-fa85-4c88-b330-31e593b8c7ed.ttl")
      (get-url "http://schema.org/activeDataRequest"))


  (.then (client/getSolidDataset "http://localhost:3000/user/oak/data-requests/data-request-e6cfe2c2-fa85-4c88-b330-31e593b8c7ec.ttl"
                                 #js {:fetch client-auth/fetch})
         (fn [dataset]
           (def ds3 dataset)))
  (-> (get-thing ds3 "http://localhost:3000/user/oak/data-requests/data-request-e6cfe2c2-fa85-4c88-b330-31e593b8c7ec.ttl")
      (client/getStringNoLocale "http://schema.org/identifier"))

  (-> (build-thing (create-thing))
      (.addUrl "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" "http://oak.se/UnemploymentCertificateDataRequest")
      (.addStringNoLocale "http://schema.org/identifier" "e6cfe2c2-fa85-4c88-b330-31e593b8c7ec")
      (.addUrl "http://purl.org/dc/elements/1.1/subject" "http://localhost:3000/user/profile/card#me")
      (.addUrl "http://schema.org/sourceOrganization" "http://localhost:3000/source/profile/card#me")
      (.addUrl "http://www.w3.org/2007/ont/link#requestedBy" "http://localhost:3000/sink/profile/card#me")
      (.build))
  
  )

(rf/reg-fx
 ::set-solid-dataset!
 (fn [{:keys [url on-success ds]}]
   (let [ds (if ds ds (client/createSolidDataset))]
     (-> (client/saveSolidDatasetAt url
                                    ds
                                    #js {:fetch client-auth/fetch})
      (.then (fn [resource]
               (rf/dispatch (merge-event-params on-success {:url url
                                                            :resource resource}))))
      (.catch #((let [code (.. % -response -status)]
                  (js/console.log "::set-solid-dataset! catch" %)
                  (js/console.log code))))))))

(rf/reg-event-fx
 ::set-solid-dataset
 (fn [_ [_ event-args]]
   {::set-solid-dataset! event-args}))

(rf/reg-fx
 ::delete-solid-dataset!
 (fn [{:keys [url on-success]}]
   (-> (client/deleteSolidDataset url
                                  #js {:fetch client-auth/fetch})
       (.then
        (fn [_]
          (rf/dispatch on-success)))
       (.catch
        (fn [err]
          (js/console.log err)
          (rf/dispatch [::delete-solid-dataset! url on-success])
          )
        )
       )))

;; Permissions

(rf/reg-fx
 ::set-solid-permissions!
 (fn [{:keys [url permissions on-success]}]
   ;; The solid client api requires a dataset with acls that exists on the server before
   ;; adding to them.
   (.then (client/getSolidDatasetWithAcl url
                                         #js {:fetch client-auth/fetch})
          (fn [resource-empty-acl]
            (let [acl (permissions->acl resource-empty-acl permissions)]
              (.then (client/saveAclFor resource-empty-acl
                                        acl
                                        #js {:fetch client-auth/fetch})
                     (fn [resource-acl]
                       (rf/dispatch (conj on-success {:url url
                                                      :resource resource-acl})))))))))


(rf/reg-event-fx
 ::set-solid-permissions
 rf/debug
 (fn [_ [_ params]]
   {::set-solid-permissions! params}))


(rf/reg-fx
 ::get-resource-info!
 (fn [{:keys [url on-success on-error]}]
   (js/console.log "::get-resource-info!" url on-success on-error)
   (-> (client/getResourceInfo url #js {:fetch client-auth/fetch})
       (.then (fn [dataset]
                (js/console.log "::get-resource-info! then" url dataset)
                (rf/dispatch on-success)))
       (.catch (fn [error]
                 (js/console.log "::get-resource-info! error" error)
                 (rf/dispatch on-error))))))
