(ns se.jobtechdev.oak.cpl.helpers
  "Helper functions"
  (:require ["pako" :as pako]
            [goog.crypt.base64 :as b64]))

(defn target-value
  "Get the value from a DOM event"
  [event]
  (some-> event .-target .-value))

(defn scroll-into-view
  "Scroll to the given element id"
  [element-id]
  (.scrollIntoView (js/document.getElementById element-id)))

(defn obj->clj
  "Convert any javascript object to a clojure map"
  [obj]
  (js->clj (js/Object.assign #js {} obj) :keywordize-keys true))

(defn current-location
  "Get current location href from the browser"
  []
  (.. js/window -location -href))

(defn current-origin
  "Get current location href from the browser"
  []
  (.. js/window -location -origin))

(defn set-interval
  "Will call the handler every timeout interval, and returns the timer id"
  [handler timeout]
  (js/setInterval handler timeout))

(defn clear-interval
  "Will clear the timer with given id"
  [id]
  (js/clearInterval id))

(defn deflate
  "Inflates a deflated b64 encoded string"
  [text]
  (pako/deflate text))



(defn inflate
  "Inflates a deflated b64 encoded string"
  [compressed]
  (let [tostring (clj->js {:to "string"})]
    (pako/inflate compressed tostring)))


(comment
  (def compressed0 "eJyFU9ty2jAQ/Rc921g2mIufkpCEwjRACmmAptMR9mKUypYjyRAnk9/qD/THKhsTO5lk+mLP7tnZs3vO6hmd+DxW8KiQ9wNtlUqkZ1n7/b6xbza4CC0H213LFxBArChh0trZyHhGNEAeOtFfA6ksgTwo/gaSiqhU6oT0txARr4w1kK7vwdc8BY1mSVLBCo7AtxSISFrHEgPFJIKqRxEZaEMiyrLxG6iWM1BIdxC/xauUoYfOl9hQEBWe5w6p4+j9LYlDCM6Jgvdb1CHdTsoUglqrQ1wCNQ4tH1E8Z5iCkDyukDI20IDvQMSRnm8iQhLTJ6JovfAT3EA3MUQJ41kOzd5Jf05Dqgg7536aw/mGBVBZcIgLE9DLz1cvv4PQmpA1g/6r8+VeJPahlMbBjmNix7Tx3HY9t+057Uaz3VnpyupgZkfXn4/NP5i4djVpiRZCVidzh7od3HWw6bZa9h36xCw9kW3inml3ai68En+q4Zu7yHtgbDq23aru8FSsQcnN3z8igoBRTRmjl5ea06WejPuEbblUXhNjbEmeCh+s/IUJzlhxZIngfFOb6iJwXNfuzWgY64UE5A/uoKDeKfhY5lziXeGRX+xwBWrLg/+N8RuyI/80FQmXOT2REkSth4Hu97kPkI2264FPJ3R0ubr4Nr+eDeUwGjrj/rC9ii6l79zoeJyRxTWdMEmX90s8ZHav0Zhcjd3R0tmJAD8y3p52x3vyZeV22MNl8+tV7wabhD8lA9Zf0CAN8fWydzHYxFE0H+6m84WTdW5v2bqVLs7M0frX1LYjEjycnWq5/wGMzpPK")
  (def compressed "eJyFU9tS4kAQ/Zd5TsgkgEKeZIO66KIgXhbWLWtImjA4ycSZCRgsf2t/YH9sJyGYaGntS1Ldp6tP9zk9L+jI57GCZ4XcX2ipVCJdy9psNo1Ns8FFaDnY7li+gABiRQmT1tpGxguiAXLRkf4aSGUJ5EHxN5BURKVSJ6S/hIi4ZayBdL4CX/MUNJolSQUrOALfUiAiae1LDBSTCKoeRWSgBYkoyy7eQbWcgUK6hvg9XqUMPXS+xIKCqPA8t0vtR/eWJA4h6BMFH7eoQ7qdlCkEtVa7uARqHFo+onjOMAIheVwhZWygU74GEUd6vksRkphuiaL1wi9wA93EECWMZzk0+SB9n4ZUEdbnfprD+YYFUFmwiwsT0OvvNy9vQWhNyJyB9+Z8uReJfSilcbDjmNgxbXxtd9x2y3W6DafbmenK6mAme9df9s0/mbh2NWmJFkJWJ3OPOoe442Cz3WrZ9+gLs/REtom7pn1Yc+GN+EsN391F3gNj07HtVnWHPTEHJRd//4gIAkY1ZYxeX2tOl3oy7hO25FK5TYyxJXkqfLDyFyY4Y8WRJYLzRW2q48Bpt+3uhIaxXkhA/uB2Cuqdgs9lziVeFx75xQ5DUEse/G+MR8j2/KNUJFzm9ERKELUeBlptch8gO1vOT316Sc9OZsdX1+PJQA6igXPhDQ5m0Yn0nRsdX2Tk55heMkmnqykeMLvbaHTvZh4ZNq+evV4q+8pchfbD3cgLxg8PdjttTmgnuP2R9DZeut0e9r5hJpvjp3T7PRiupsNgukjgcX5+/kTiJEuf6d1ocXJ60O9puf8BxMWVMA==")
  (def decoded (b64/decodeString compressed))
  (def decoded0 (js/atob compressed))
  #_(.stringify js/JSON tostring)
  (def json1 (clj->js {:foo 1 :bar 2}))
  (def t "testing")
  (def json "{\"foo\": 1, \"bar\": 2, \"baz\": [1,2,3]}")
  (.stringify js/JSON json1)
  (def d (deflate (.stringify js/JSON json1)))
  (def i (inflate d))
  (def uncompressed (inflate decoded))



  (def a (.parse js/JSON i))
  (.stringify js/JSON a)
  )